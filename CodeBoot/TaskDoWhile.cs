﻿using System;

namespace CodeBoot
{
    class TaskDoWhile
    {
        static void Main(string[] args)
        {
            
        }
        public static void FirstTask()
        {
            int quantity = 0;
            int number = 0;
            int i = 1;
            Random rnd = new Random();
            do
            {
                number = rnd.Next(3, 9);
                Console.WriteLine("На этаже: " + i + "\t Человек: " + number);
                quantity += number;
                i++;
            }
            while (i <= 45);
            Console.WriteLine("Всего людей в доме: " + quantity);
        }
        public static void SecondTask()
        {
            int car = 0;
            int overall = 0;
            Random rnd = new Random();
            do
            {
                car = 0;
                overall++;
                car += 7;
                if(car >= 20)
                {
                    overall -= 1;
                    car += 1;
                }
            }
            while (overall <= 100);
            Console.WriteLine(car);
        }
        public static void ThirdTask()
        {
            int days = 88;
            int Guest = 0;
            Random rnd = new Random();
            do
            {
                days--;
                Guest += rnd.Next(5, 17);
                if(Guest >= 200)
                {
                    Guest -= 100;
                    Console.WriteLine("Петр удалил 100 гостей");
                }
                if (Guest == 173)
                { 
                    Console.WriteLine("Эльза разорвала помолвку");
                    break;
                }
                Console.WriteLine(Guest);
            }
            while (true);
        }
        public static void FourthTask()
        {
            int floors = 33;
            double soldires = 1000;
            for (int i = 0; i < 33; i++)
            {
                soldires = Math.Round(soldires, 0);
                floors--;
                if (soldires <= 0)
                    break;
                if((floors & 2) != 0)
                {
                    soldires *= 0.9;
                }
                if((floors & 2) == 0 && soldires <= 66)
                {
                    break;
                }
                if ((floors & 2) == 0)
                    soldires -= 66;
            }
            Console.WriteLine("Солдат осталось : " + soldires + " Этаж :" + floors);
        }
    }
}
